var breakpoints = {large: 1024, medium: 640};

$(function() {

	// INIT FUNCTIONS
	$(window).load(function () {
		equalHeights('.equal-height');
	}).resize(function () {
		equalHeights('.equal-height');
	}).resize();

	// EXTERNAL LINKS
	$('[rel=external]').click( function(e) {
		e.preventDefault();
		window.open( $(this).attr('href') );
	});

	// SVG Fallback to .png
	if (!Modernizr.svg) {
		$('.svg-fallback').each( function() {
			$(this).attr('src', $(this).data('src'));
		});
	}

	/*// SLIDESHOWS
	$('.main-slide').flexslider({
    	animation: "slide",
    	prevText: "",
    	nextText: ""
  	});*/

	// EQUAL HEIGHT COLUMNS
	/*$(window).load(function(){
		$('.equal-heights').eqHeights();
	});*/
	
});

// Equal Heights within a row
function equalHeights(target) {
	var $cols = $(target),
		$rows = $(target).closest('.row'),
		mh = 0;

	$rows.each(function() {
		$(this).find('.equal-height').css('height', 'auto').each(function () {
			if($(window).width() > breakpoints.medium) {
				var th = $(this).height();
				if (th > mh)
					mh = th;
			} else {
				mh = 'auto';
			}
		}).height(mh);
		mh = 0;
	});
}

function equalHeights(target) {
	var $cols = $(target),
		$rows = $(target).closest('.row'),
		mh = 0;

	$rows.each(function() {
		$(this).find($cols).css('height', 'auto').each(function () {
			if($(window).width() > breakpoints.medium) {
				var th = $(this).height();
				if (th > mh)
					mh = th;
			} else {
				mh = 'auto';
			}
		}).height(mh);
		mh = 0;
	});
}

//EQUAL HEIGHTS
/*(function ($) {

  $.fn.eqHeights = function () {

    var el = $(this);
    if (el.length > 0 && !el.data('eqHeights')) {
      $(window).bind('resize.eqHeights', function () {
        el.eqHeights();
      });
      el.data('eqHeights', true);
    }
    return el.each(function () {
		var curHighest = 0;
		$(this).children().children().each(function () {
			if($(window).width() > 767) {
				var el = $(this),
					elHeight = el.height('auto').height();
				if (elHeight > curHighest) {
					curHighest = elHeight;
				}
			} else {
				curHighest = 'auto';
			}
		}).height(curHighest);
    });
  };

}(jQuery));*/