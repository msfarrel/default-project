	    </div><!-- content -->
	    
	    <footer class="footer">
	    
	    </footer><!-- footer -->
	</div><!-- container -->	

	<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/javascripts/vendor/jquery-1.10.2.min.js"><\/script>')</script>

	<script src="assets/javascripts/vendor/jquery.validate.min.js"></script>
	<script src="assets/javascripts/vendor/jquery.flexslider-min.js"></script>
	<script src="assets/javascripts/vendor/jquery.magnific-popup.min.js"></script>
	<script src="assets/javascripts/forms.js"></script>
	<script src="assets/javascripts/scripts.js"></script>

</body>
</html>